/*
  Thingy Axon - Sonoff Dual

  This axon runs on Sonoff Dual (http://sonoff.itead.cc/en/products/sonoff/sonoff-dual) and connects to Thingy system

  Copyright (c) 2017 Thingy.IO
*/

#define   PIN_LED     13

#include <Thingy.h>
#include <Ticker.h>
#include <Button.h>

Thingy::Axon sonoff;
Ticker ticker;

bool state1 = false, state2 = false;
bool state1_ = false, state2_ = false;

bool load_state()
{
  File stateFile = SPIFFS.open("/state.json", "r");
  if (!stateFile)
  {
    LOG << F("Failed to open state file  ");
    return false;
  }

  size_t size = stateFile.size();
  if (size > 255)
  {
    LOG << F("State file size is too large ");
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  stateFile.readBytes(buf.get(), size);

  StaticJsonBuffer<255> settingsBuffer;
  JsonObject& stateObject = settingsBuffer.parseObject(buf.get());

  stateFile.close();

  if (!stateObject.success())
  {
    LOG << F("Failed to parse State file ");
    return false;
  }

  state1_      = stateObject.get<bool>("S1");
  state2_      = stateObject.get<bool>("S2");

  return true;
}

bool save_state()
{
  StaticJsonBuffer<255> settingsBuffer;
  JsonObject& stateObject = settingsBuffer.createObject();

  stateObject["S1"] = state1;
  stateObject["S2"] = state2;

  File stateFile = SPIFFS.open("/state.json", "w+");
  if (!stateFile)
  {
    LOG << F("Failed to open state file for writing ");
    return false;
  }

  stateObject.printTo(stateFile);
  stateFile.close();

  return true;
}

void set_state()
{
    if (state1 != state1_)
    {
      state1 = state1_;
      unsigned char state = 0 + (state1 ? 1 : 0) + (state2 ? 2 : 0);
      
      save_state();
      LOG << (state1 ? F("ON 1"): F("OFF 1")) << endl;

      digitalWrite(PIN_LED, LOW); delay(50); digitalWrite(PIN_LED, HIGH);
      
      Serial.flush();
      Serial.write(0xA0);
      Serial.write(0x04);
      Serial.write(state);
      Serial.write(0xA1);
      Serial.flush();

      sonoff["State 1"].put(state1 ? "ON" : "OFF", 1, true);
    }
    if (state2 != state2_)
    {
      state2 = state2_;
      unsigned char state = 0 + (state1 ? 1 : 0) + (state2 ? 2 : 0);
      
      save_state();
      LOG << (state2 ? F("ON 2"): F("OFF 2")) << endl;

      digitalWrite(PIN_LED, LOW); delay(50); digitalWrite(PIN_LED, HIGH);
      
      Serial.flush();
      Serial.write(0xA0);
      Serial.write(0x04);
      Serial.write(state);
      Serial.write(0xA1);
      Serial.flush();

      sonoff["State 2"].put(state2 ? "ON" : "OFF", 2, true);
    }
}

void tick()
{
  int state = digitalRead(PIN_LED);
  digitalWrite(PIN_LED, !state);
}

void setup()
{
	// Setup console
	Serial.begin(19230);
	delay(10);

	LOG << endl << endl  << F("Sonoff.Dual") << " - " << __DATE__ << " " << __TIME__ << endl << "#" << endl << endl;

  // LED
  pinMode(PIN_LED, OUTPUT);
  ticker.attach(0.6, tick);

    // Thingy
  sonoff.setup();

  // Persisted state
  load_state();
  set_state();

  if (!sonoff.begin())
  {
    LOG << F("Sonoff.Dual - Fatal error. Please restart.") << endl << endl << endl;
    sonoff.restart();
  }

  ticker.detach();
  digitalWrite(PIN_LED, HIGH);

	// setting handles
  sonoff["Command 1"].setHandle([](const Thingy::Stream& sender, const String& value){
    if (value == "ON") state1_ = true;
    if (value == "OFF") state1_ = false;
    if (value == "TOGGLE") state1_ = !state1;
	});
  sonoff["Command 2"].setHandle([](const Thingy::Stream& sender, const String& value){
    if (value == "ON") state2_ = true;
    if (value == "OFF") state2_ = false;
    if (value == "TOGGLE") state2_ = !state2;
	});
}

void loop()
{
  sonoff.loop();
  set_state();

  if (Serial.available() >= 4) 
  {
    unsigned char value;
    if (Serial.read() == 0xA0) 
    {
      if (Serial.read() == 0x04) 
      {
        value = Serial.read();
        if (Serial.read() == 0xA1) 
        {
          if ((value & (1 << 0)) == 1)
          {
            // 1 turned on 
            digitalWrite(PIN_LED, LOW); delay(50); digitalWrite(PIN_LED, HIGH); delay(50);
          }
          if ((value & (1 << 0)) == 0)
          {
            // 1 turned off 
            digitalWrite(PIN_LED, LOW); delay(50); digitalWrite(PIN_LED, HIGH); delay(50);
          }
          if ((value & (1 << 1)) == 1)
          {
            // 2 turned on 
            digitalWrite(PIN_LED, LOW); delay(50); digitalWrite(PIN_LED, HIGH); delay(50);
          }
          if ((value & (1 << 1)) == 0)
          {
            // 2 turned off 
            digitalWrite(PIN_LED, LOW); delay(50); digitalWrite(PIN_LED, HIGH); delay(50);
          }
        }
      }
    }
  }
}