# Sonoff SC - Thingy firmware #

This folder contains firmware for [Sonoff SC](http://sonoff.itead.cc/en/products/residential/sonoff-sc) devices. Firmware is customized for usage with [Thingy](http://thingy.io) system and havily based on [Xose Peres project](https://github.com/xoseperez/sonoffsc).

## Device specification ##
* Power Supply: USB 5V
* Wireless Standard: Wi-Fi 2.4GHz b/g/n
* Security Mechanism: WEP/WPA-PSK/WPA2-PSK
* Operating Temp.: 0℃~70℃
* Operating Humidity: ≤80%
* Material: FR-ABS
* For indoor use

## Flashing ##

Follow detailed steps described by [TinkerMan](http://tinkerman.cat/sonoff-sc-with-mqtt-and-domoticz-support/)

## Home Assistant Integration ##
In order to integrate the device with [Home Assistant](https://home-assistant.io/) system it is necessery to configure **MQTT Component** in Home Assistant configuration and add the following **MQTT Sensor component** for each of the parameters (temperature, humidity, lightness, noise,dust):

```
sensor:
  - platform: mqtt
    state_topic:   "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>"
    name: '<Parameter>'
    unit_of_measurement: '<Unit>'
```

## Additional info and documentation

Firmware is split into 2 firmwares - [ESP8266](esp8266/) and [ATMega](atmega/).

More information about the **Sonoff** system you can find [here](http://sonoff.itead.cc) and detailed wiki pages [here](https://www.itead.cc/wiki/Product).

More information about the **Thingy** system you can under [http://thingy.io](http://thingy.io) and detailed documentation under [http://docs.thingy.io](http://docs.thingy.io).