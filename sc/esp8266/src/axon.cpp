/*
  Axon - Sonoff SC

  This axon runs on Sonoff SC (http://sonoff.itead.cc/en/products/residential/sonoff-sc) and connects to Thingy system

  Copyright (c) 2017 Thingy.IO
*/

#define PIN_BUTTON 0
#define PIN_LED 13		// active on LOW

#include <Thingy.h>
#include <Ticker.h>
#include <Button.h>
#include <SerialLink.h>

Thingy::Axon sonoff;
Ticker ticker;
Button button(PIN_BUTTON);
SerialLink link(Serial, false);

const PROGMEM char at_push[] = "AT+PUSH";
const PROGMEM char at_every[] = "AT+EVERY";
const PROGMEM char at_temp[] = "AT+TEMP";
const PROGMEM char at_hum[] = "AT+HUM";
const PROGMEM char at_dust[] = "AT+DUST";
const PROGMEM char at_noise[] = "AT+NOISE";
const PROGMEM char at_light[] = "AT+LIGHT";
const PROGMEM char at_clap[] = "AT+CLAP";
const PROGMEM char at_code[] = "AT+CODE";
const PROGMEM char at_thld[] = "AT+THLD";

float temperature;
int humidity;
int light;
float dust;
int noise;

bool commsGet(char * key) {
    return false;
}

bool commsSet(char * key, int value)
{
    if (strcmp_P(key, at_code) == 0)
    {
        Serial << "[Clap]" << endl;
        sonoff["Clap"].put(String(value, DEC));
        return true;
    }

    if (strcmp_P(key, at_temp) == 0)
    {
        Serial << "[Temperature]" << endl;
        temperature = (float) value / 10;
        sonoff["Temperature"].put(String(temperature,1));
        return true;
    }

    if (strcmp_P(key, at_hum) == 0)
    {
        Serial << "[Humidity]" << endl;
        humidity = value;
        sonoff["Humidity"].put(String(humidity,DEC));
        return true;
    }

    if (strcmp_P(key, at_light) == 0)
    {
        Serial << "[Light]" << endl;
        light = value;
        sonoff["Light"].put(String(light,DEC));
        return true;
    }

    if (strcmp_P(key, at_dust) == 0)
    {
        Serial << "[Dust]" << endl;
        dust = (float) value / 100;
        sonoff["Dust"].put(String(dust,2));
        return true;
    }

    if (strcmp_P(key, at_noise) == 0)
    {
        Serial << "[Noise]" << endl;
        noise = value;
        sonoff["Noise"].put(String(noise,DEC));
        return true;
    }

    return false;
}

void tick()
{
  int state = digitalRead(PIN_LED);
  digitalWrite(PIN_LED, !state);
}

void handle_command(const Thingy::Stream& stream, const String& value)
{

}

void setup()
{
	// Setup console
	Serial.begin(115200);
	delay(10);

	LOG << endl << endl  << F("Sonoff.SC") << " - " << __DATE__ << " " << __TIME__ << endl << "#" << endl << endl;

  // LED
  pinMode(PIN_LED, OUTPUT);
  ticker.attach(0.6, tick);

  // Button
  button.begin();

  // Thingy
  sonoff.setup();

  if (!sonoff.begin())
	{
		LOG << F("Sonoff.SC - Fatal error. Please restart.") << endl << endl << endl;
		return;
	}

  ticker.detach();
  digitalWrite(PIN_LED, HIGH);

  link.onGet(commsGet);
  link.onSet(commsSet);

  //link.send_P(at_every, 60);
  link.send_P(at_clap, 1);
  link.send_P(at_push, 1);
}

void loop()
{
	sonoff.loop();
  link.handle();
}
