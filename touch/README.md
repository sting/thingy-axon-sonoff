# Sonoff Touch - Thingy firmware #

This folder contains firmware for [Sonoff Touch](http://sonoff.itead.cc/en/products/residential/sonoff-touch) devices. Firmware is customized for usage with [Thingy](http://thingy.io) system.

## Device specification ##
* Power Supply: 90V~250V AC
* Max. Current: 2A
* Wireless Standard: Wi-Fi 2.4GHz b/g/n
* Security Mechanism: WEP/WPA-PSK/WPA2-PSK
* Operating Temp.: -20℃~70℃
* Operating Humidity: ≤80%
* Material: FR-ABS + Toughened glass panel
* Connector: universal

## Flashing ##

In order to flash this firmware, connect the device using diagram bellow. In order to enter flash mode, it is necessary to pull GPIO0 pin to GND and start up device / provide power.

![](extras/sonoff_touch_pcb.jpg)

## Home Assistant Integration ##
In order to integrate the device with [Home Assistant](https://home-assistant.io/) system it is necessery to configure **MQTT Component** in Home Assistant configuration and add the following **MQTT Light component**:

```
light:
  - platform: mqtt
    command_topic: "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'Command' stream
    state_topic:   "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'State' stream
```

## Additional info and documentation

More information about the **Sonoff** system you can find [here](http://sonoff.itead.cc) and detailed wiki pages [here](https://www.itead.cc/wiki/Product).

More information about the **Thingy** system you can under [http://thingy.io](http://thingy.io) and detailed documentation under [http://docs.thingy.io](http://docs.thingy.io).

## Credits & licenses

Copyright (c) 2017 Thingy.IO
