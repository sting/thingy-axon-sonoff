/*
  Thingy Axon - Sonoff 4CH

  This axon runs on Sonoff 4CH (http://sonoff.itead.cc/en/products/sonoff/sonoff-4ch) and connects to Thingy system

  Copyright (c) 2017 Thingy.IO
*/

#define   PIN_LED                  13

#define   PIN_1_BUTTON             0
#define   PIN_1_RELAY_LED          12

#define   PIN_2_BUTTON             9
#define   PIN_2_RELAY_LED          5

#define   PIN_3_BUTTON             10
#define   PIN_3_RELAY_LED          4

#define   PIN_4_BUTTON             14
#define   PIN_4_RELAY_LED          15

#include <Thingy.h>
#include <Ticker.h>
#include <Button.h>

Thingy::Axon sonoff;
Ticker ticker;
Button button1(PIN_1_BUTTON), button2(PIN_2_BUTTON), button3(PIN_3_BUTTON), button4(PIN_4_BUTTON);

bool state1 = false, state2 = false, state3 = false, state4 = false;
bool state1_ = false, state2_ = false, state3_ = false, state4_ = false;

bool load_state()
{
  File stateFile = SPIFFS.open("/state.json", "r");
  if (!stateFile)
  {
    Serial << F("Failed to open state file  ");
    return false;
  }

  size_t size = stateFile.size();
  if (size > 255)
  {
    Serial << F("State file size is too large ");
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  stateFile.readBytes(buf.get(), size);

  StaticJsonBuffer<255> settingsBuffer;
  JsonObject& stateObject = settingsBuffer.parseObject(buf.get());

  stateFile.close();

  if (!stateObject.success())
  {
    Serial << F("Failed to parse State file ");
    return false;
  }

  state1_      = stateObject.get<bool>("S1");
  state2_      = stateObject.get<bool>("S2");
  state3_      = stateObject.get<bool>("S3");
  state4_      = stateObject.get<bool>("S4");

  return true;
}

bool save_state()
{
  StaticJsonBuffer<255> settingsBuffer;
  JsonObject& stateObject = settingsBuffer.createObject();

  stateObject["S1"] = state1;
  stateObject["S2"] = state2;
  stateObject["S3"] = state3;
  stateObject["S4"] = state4;

  File stateFile = SPIFFS.open("/state.json", "w+");
  if (!stateFile)
  {
    LOG << F("Failed to open state file for writing ");
    return false;
  }

  stateObject.printTo(stateFile);
  stateFile.close();

  return true;
}

void set_state()
{
    if (state1 != state1_)
    {
      state1 = state1_;
      save_state();
      Serial << (state1 ? F("ON 1"): F("OFF 1")) << endl;
      digitalWrite(PIN_1_RELAY_LED, state1 ? HIGH : LOW);
      sonoff["State 1"].put(state1 ? "ON" : "OFF", 1, true);
    }
    if (state2 != state2_)
    {
      state2 = state2_;
      save_state();
      Serial << (state2 ? F("ON 2"): F("OFF 2")) << endl;
      digitalWrite(PIN_2_RELAY_LED, state2 ? HIGH : LOW);
      sonoff["State 2"].put(state2 ? "ON" : "OFF", 2, true);
    }
    if (state3 != state3_)
    {
      state3 = state3_;
      save_state();
      Serial << (state3 ? F("ON 3"): F("OFF 3")) << endl;
      digitalWrite(PIN_3_RELAY_LED, state3 ? HIGH : LOW);
      sonoff["State 3"].put(state3 ? "ON" : "OFF", 3, true);
    }
    if (state4 != state4_)
    {
      state4 = state4_;
      save_state();
      Serial << (state4 ? F("ON 4"): F("OFF 4")) << endl;
      digitalWrite(PIN_4_RELAY_LED, state4 ? HIGH : LOW);
      sonoff["State 4"].put(state4 ? "ON" : "OFF", 4, true);
    }
}

void tick()
{
  int state = digitalRead(PIN_LED);
  digitalWrite(PIN_LED, !state);
}

void setup()
{
	// Setup console
	Serial.begin(115200);
	delay(10);

	Serial << endl << endl  << F("Sonoff.4CH") << " - " << __DATE__ << " " << __TIME__ << endl << "#" << endl << endl;

  // LED
  pinMode(PIN_LED, OUTPUT);
  ticker.attach(0.6, tick);

  // Buttons
  button1.begin();
  button2.begin();
  button3.begin();
  button4.begin();

  // Relays
  pinMode(PIN_1_RELAY_LED, OUTPUT);
  pinMode(PIN_2_RELAY_LED, OUTPUT);
  pinMode(PIN_3_RELAY_LED, OUTPUT);
  pinMode(PIN_4_RELAY_LED, OUTPUT);

  // Thingy
  sonoff.setup();

  // Persisted state
  load_state();
  set_state();

  if (!sonoff.begin())
  {
    Serial << F("Sonoff.4CH - Fatal error. Please restart.") << endl << endl << endl;
    return;
  }

  ticker.detach();

	// setting handles
  sonoff["Command 1"].setHandle([](const Thingy::Stream& sender, const String& value){
    if (value == "ON") state1_ = true;
    if (value == "OFF") state1_ = false;
    if (value == "TOGGLE") state1_ = !state1;
	});
  sonoff["Command 2"].setHandle([](const Thingy::Stream& sender, const String& value){
    if (value == "ON") state2_ = true;
    if (value == "OFF") state2_ = false;
    if (value == "TOGGLE") state2_ = !state2;
	});
  sonoff["Command 3"].setHandle([](const Thingy::Stream& sender, const String& value){
    if (value == "ON") state3_ = true;
    if (value == "OFF") state3_ = false;
    if (value == "TOGGLE") state3_ = !state3;
	});
  sonoff["Command 4"].setHandle([](const Thingy::Stream& sender, const String& value){
    if (value == "ON") state4_ = true;
    if (value == "OFF") state4_ = false;
    if (value == "TOGGLE") state4_ = !state4;
	});
}

void loop()
{
  sonoff.loop();

  if (button1.released()) state1_ = !state1;
  if (button2.released()) state2_ = !state2;
  if (button3.released()) state3_ = !state3;
  if (button4.released()) state4_ = !state4;

  set_state();
}
