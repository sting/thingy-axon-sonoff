# Sonoff 4CH - Thingy firmware #

This folder contains firmware for [Sonoff 4CH](http://sonoff.itead.cc/en/products/sonoff/sonoff-4ch) devices. Firmware is customized for usage with [Thingy](http://thingy.io) system.

## Device specification ##
* Power Supply: 90~250V AC
* Rated frequency: 50HZ/60HZ
* Max current: 10A
* Max Power: 2200W
* Plug: 2 prong Chinese plug (type A )
* Wireless Standard: Wi-Fi 2.4GHz b/g/n
* Security Mechanism: WEP/WPA-PSK/WPA2-PSK

## Flashing ##

In order to flash this firmware, connect the device using diagram bellow. In order to enter flash mode, it is necessary to hold push button "1" down and start up device / provide power (push button can be released after that).

![](extras/sonoff_4ch_pcb.jpg)

## Home Assistant Integration ##
In order to integrate the device with [Home Assistant](https://home-assistant.io/) system it is necessery to configure **MQTT Component** in Home Assistant configuration and add the following **MQTT Switch component** for each of the channels:

```
switch:
  - platform: mqtt
    command_topic: "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'Command' stream
    state_topic:   "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'State' stream
    retain:        true
```

## Additional info and documentation

Additional pictures you can find [here](extras/).

More information about the **Sonoff** system you can find [here](http://sonoff.itead.cc) and detailed wiki pages [here](https://www.itead.cc/wiki/Product).

More information about the **Thingy** system you can under [http://thingy.io](http://thingy.io) and detailed documentation under [http://docs.thingy.io](http://docs.thingy.io).

## Credits & licenses

Copyright (c) 2017 Thingy.IO
