# Sonoff firmwares #

This repository contains firmwares for [Sonoff](http://sonoff.itead.cc) devices. Firmwares are customized for usage with [Thingy](http://thingy.io) system.


### Supported devices ###

* Sonoff Basic [(product)](http://sonoff.itead.cc/en/products/sonoff/sonoff-basic) [(wiki)](https://www.itead.cc/wiki/Sonoff)
* Sonoff Dual [(product)](http://sonoff.itead.cc/en/products/sonoff/sonoff-dual) [(wiki)](https://www.itead.cc/wiki/Sonoff_Dual)
* Sonoff Pow [(product)](http://sonoff.itead.cc/en/products/sonoff/sonoff-pow) [(wiki)](https://www.itead.cc/wiki/Sonoff_Pow)
* Sonoff Slampher [(product)](http://sonoff.itead.cc/en/products/residential/slampher-rf) [(wiki)](https://www.itead.cc/wiki/Slampher)
* Sonoff 4CH [(product)](http://sonoff.itead.cc/en/products/sonoff/sonoff-4ch) [(wiki)](https://www.itead.cc/wiki/Sonoff_4CH)
* Sonoff SC [(product)](http://sonoff.itead.cc/en/products/residential/sonoff-sc) [(wiki)](https://www.itead.cc/wiki/Sonoff_SC)
* Sonoff Touch [(product)](http://sonoff.itead.cc/en/products/residential/sonoff-touch)

## Additional info and documentation

More information about the **Sonoff** system you can find [here](http://sonoff.itead.cc) and detailed wiki pages [here](https://www.itead.cc/wiki/Product).

More information about the **Thingy** system you can under [http://thingy.io](http://thingy.io) and detailed documentation under [http://docs.thingy.io](http://docs.thingy.io).

## Credits & licenses

Copyright (c) 2017 Thingy.IO
