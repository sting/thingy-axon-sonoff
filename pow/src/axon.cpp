/*
  Thingy Axon - Sonoff Pow

  This axon runs on Sonoff Pow (http://sonoff.itead.cc/en/products/sonoff/sonoff-pow) and connects to Thingy system

  Copyright (c) 2017 Thingy.IO
*/

#define   PIN_BUTTON   0
#define   PIN_LED      15
#define   PIN_RELAY    12

#define   PIN_CF1      13
#define   PIN_CF       14
#define   PIN_SEL      5

#define I_R       0.001
#define U_R_UP    ( 5 * 470000 ) // Real: 2280k
#define U_R_DOWN  ( 1000 )       // Real 1.009k

#define SAMPLE_RATE       1     // 1s
#define PUBLISH_RATE      60    // 1min (should be multiplier of sample rate)

#define P_MIN             5     // 5 W
#define P_MAX             2500  // 2.5 kW
#define I_MIN             0.05  // 50 mA
#define I_MAX             10    // 10 A

#include <Thingy.h>
#include <Ticker.h>
#include <Button.h>
#include <HLW8012.h>

typedef enum { NONE, ON, OFF, TOGGLE } Action;

Thingy::Axon sonoff;
Ticker ticker;

Button button(PIN_BUTTON);
Action action = NONE;

static bool onoff;

HLW8012 hlw8012;

static unsigned long sample_ts = 0;
static unsigned long samples = 0;

static unsigned long publish_ts = 0;

unsigned int P, P_ = 0, P__ = 0;    // power (active) [W]
static unsigned long P_SUM = 0;

unsigned int U, U_ = 0, U__ = 0;    // voltage [V]
static unsigned long U_SUM = 0;

double I, I_ = 0, I__ = 0;          // current [A]
static double I_SUM = 0;

unsigned int S;                     // power (apparent) [VA]
unsigned int Q;                     // power (reactive) [var]
unsigned int cosF;                  // power factor [%]
double E;                           // energy [kWh]
static double E_on_off = 0;


void tick()
{
  int state = digitalRead(PIN_LED);
  digitalWrite(PIN_LED, !state);
}

void on()
{
  Serial << F("ON") << endl;

  E_on_off = 0;
  sonoff["E"].put(String(E_on_off, 5), 1, true);

  digitalWrite(PIN_RELAY, HIGH);
  onoff = true;
  SPIFFS.rename("/state.off", "/state.on");
  sonoff["State"].put("ON", 1, true);
}

void off()
{
  Serial << F("OFF") << endl;

  digitalWrite(PIN_RELAY, LOW);
  onoff = false;
  SPIFFS.rename("/state.on", "/state.off");
  sonoff["State"].put("OFF", 1, true);
}

void toggle()
{
  Serial << F("TOGGLE") << endl;
  digitalRead(PIN_RELAY) == HIGH ? off() : on();
}

void handle_command(const Thingy::Stream& stream, const String& value)
{
  if (value == "ON")
    action = ON;
  if (value == "OFF")
    action = OFF;
  if (value == "TOGGLE")
    action = TOGGLE;
}

void cf1_interrupt() 
{
  hlw8012.cf1_interrupt();
}

void cf_interrupt() 
{
  hlw8012.cf_interrupt();
}

bool load_calibration()
{
  File calibrationFile = SPIFFS.open("/calibration.json", "r");
  if (!calibrationFile)
  {
    LOG << F("Failed to open calibration file");
    return false;
  }

  size_t size = calibrationFile.size();
  if (size > 255)
  {
    LOG << F("Calibration file size is too large ");
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  calibrationFile.readBytes(buf.get(), size);

  StaticJsonBuffer<255> buffer;
  JsonObject& calibrationObject = buffer.parseObject(buf.get());

  calibrationFile.close();

  if (!calibrationObject.success())
  {
    LOG << F("Failed to parse calibration file ");
    return false;
  }

  int p = calibrationObject.get<int>("P");
  if (p>0) hlw8012.setPowerMultiplier(p);
  
  int i = calibrationObject.get<int>("I");
  if (i>0) hlw8012.setCurrentMultiplier(i);
  
  int u = calibrationObject.get<int>("U");
  if (u>0) hlw8012.setVoltageMultiplier(u);

  return true;
}

bool save_calibration()
{
  StaticJsonBuffer<255> buffer;
  JsonObject& calibrationObject = buffer.createObject();

  calibrationObject["P"] = hlw8012.getPowerMultiplier();
  calibrationObject["I"] = hlw8012.getCurrentMultiplier();
  calibrationObject["U"] = hlw8012.getVoltageMultiplier();

  File calibrationFile = SPIFFS.open("/calibration.json", "w+");
  if (!calibrationFile)
  {
    LOG << F("Failed to open calibration file for writing ");
    return false;
  }

  calibrationObject.printTo(calibrationFile);
  calibrationFile.close();

  return true;
}


void colibration_power(unsigned int power) {
  if (power > 0) {
      hlw8012.expectedActivePower(power);
      save_calibration();
  }
}

void colibration_current(double current) {
  if (current > 0) {
      hlw8012.expectedCurrent(current);
      save_calibration();
  }
}

void colibration_voltage(unsigned int voltage) {
  if (voltage > 0) {
      hlw8012.expectedVoltage(voltage);
      save_calibration();
  }
}

void calibration_reset() {
  hlw8012.resetMultipliers();
  save_calibration();
}

void setup()
{
	// Setup console
	Serial.begin(115200);
	delay(10);

	Serial << endl << endl  << F("Sonoff.POW") << " - " << __DATE__ << " " << __TIME__ << endl << "#" << endl << endl;

  // LED
  pinMode(PIN_LED, OUTPUT);
  ticker.attach(0.6, tick);

  // Button
  button.begin();

  // Relay
  pinMode(PIN_RELAY, OUTPUT);

  // HLW8012
  hlw8012.begin(PIN_CF, PIN_CF1, PIN_SEL, HIGH, true);
  hlw8012.setResistors(I_R, U_R_UP, U_R_DOWN);

  //load_calibration();

  //colibration_voltage(238);
  //colibration_current()
  //save_calibration();

  // Thingy
  sonoff.setup();

  // Persisted state
  onoff = false;

  if (SPIFFS.exists("/state.on"))
  {
    Serial << F("Sonoff.POW - ON") << endl;
    onoff = true;
    digitalWrite(PIN_RELAY, HIGH);
  }

  if (SPIFFS.exists("/state.off"))
  {
    Serial << F("Sonoff.POW - OFF") << endl;
    onoff = false;
    digitalWrite(PIN_RELAY, LOW);
  }

  if (!sonoff.begin())
  {
    Serial << F("Sonoff.POW - Fatal error. Please restart.") << endl << endl << endl;
    sonoff.restart();
  }

  ticker.detach();
  digitalWrite(PIN_LED, LOW);

	// setting handles
	sonoff["Command"].setHandle(handle_command);

  action = onoff ? ON : OFF;

  attachInterrupt(PIN_CF1, cf1_interrupt, CHANGE);
  attachInterrupt(PIN_CF, cf_interrupt, CHANGE);

  /*
    detachInterrupt(PIN_CF1);
    detachInterrupt(PIN_CF);
  */  
}


inline Thingy::Stream &operator <<(Thingy::Stream &obj, const String &arg)
{ obj.put(arg, 1, true); return obj; } 

void loop()
{
  sonoff.loop();
  
  if (millis() - sample_ts > SAMPLE_RATE * 1000)
  {
    sample_ts = millis();
    samples++;

    P = hlw8012.getActivePower();
    U = hlw8012.getVoltage();
    I = hlw8012.getCurrent();
    S = hlw8012.getApparentPower();
    Q = hlw8012.getReactivePower();
    cosF = 100 * hlw8012.getPowerFactor();

    //Serial << "U=" << String(U, DEC) << "V - " << "I=" << String(I, 2) << "A - " << "P=" << String(P, DEC) << "W - " << "cosF=" << String(cosF, DEC) << "% - " <<"S=" << String(S, DEC) << "VA - " << "Q=" << String(Q, DEC) << "var" << endl;

    if (P_MIN > P || P > P_MAX) P = 0; // power limitation
    if (I_MIN > I || I > I_MAX) I = 0; // current limitation
    if (P_MIN > S || S > P_MAX) S = 0; // power limitation
    if (P_MIN > Q || Q > P_MAX) Q = 0; // power limitation

    if (P == 0 && P_ > 0 && P__ == 0) P_SUM = P_SUM - P_; // spike
    P__ = P_; P_  = P;

    if (I == 0 && I_ > 0 && I__ == 0) I_SUM = I_SUM - I_; // spike
    I__ = I_; I_  = I;

    if (U == 0 && U_ > 0 && U__ == 0) U_SUM = U_SUM - U_; // spike
    U__ = U_; U_  = U;


    if (samples * SAMPLE_RATE >= PUBLISH_RATE)
    {

      P = P_SUM / samples; // power (active)
      I = I_SUM / samples; // current
      U = U_SUM / samples; // voltage

      S = I * U;
      Q = (S > P) ? sqrt(S * S - P * P) : 0;
      cosF = (S > 0) ? 100 * P / S : 100;
      if (cosF > 100) cosF = 100;

      E = (double) P * (millis() - publish_ts) / 1000.0 / 60.0 / 60.0 ;
      E_on_off += E;

      P_SUM = I_SUM = U_SUM = 0;
      samples = 0;
      publish_ts = millis();

      sonoff["I"] << String(I,2);
      sonoff["U"].put(String(U, DEC), 1, true);
      sonoff["P"].put(String(P, DEC), 1, true);
      sonoff["S"].put(String(S, DEC), 1, true);
      sonoff["Q"].put(String(Q, DEC), 1, true);
      sonoff["cosF"].put(String(cosF, DEC), 1, true);
      sonoff["E"].put(String(E_on_off, 5), 1, true);
    }

    P_SUM = P_SUM + P_;
    I_SUM = I_SUM + I_;
    U_SUM = U_SUM + U_;
  }

  if (button.released())
    action = TOGGLE;

  if (action == ON)
    on();
  if (action == OFF)
    off();
  if (action == TOGGLE)
    toggle();

  if (action != NONE)
    action = NONE;
}
