# Sonoff Basic - Thingy firmware #

This folder contains firmware for [Sonoff Basic](http://sonoff.itead.cc/en/products/sonoff/sonoff-basic) devices. Firmware is customized for usage with [Thingy](http://thingy.io) system.

## Device specification ##
* Power Supply: 90V~250V AC
* Max. Current: 10A
* Wireless Standard: Wi-Fi 2.4GHz b/g/n
* Security Mechanism: WEP/WPA-PSK/WPA2-PSK
* Operating Temp.: -20℃~70℃
* Operating Humidity: ≤80%
* Material: RF-ABS
* Connector: universal

## Flashing ##

In order to flash this firmware, connect the device using diagram bellow. In order to enter flash mode, it is necessary to hold push button down and start up device / provide power (push button can be released after that).

![](extras/sonoff_basic_pcb.jpg)

## Home Assistant Integration ##
In order to integrate the device with [Home Assistant](https://home-assistant.io/) system it is necessery to configure **MQTT Component** in Home Assistant configuration and add the following **MQTT Light component**:

```
light:
  - platform: mqtt
    command_topic: "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'Command' stream
    state_topic:   "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'State' stream
```

## Additional info and documentation

Additional pictures you can find [here](extras/).

More information about the **Sonoff** system you can find [here](http://sonoff.itead.cc) and detailed wiki pages [here](https://www.itead.cc/wiki/Product).

More information about the **Thingy** system you can under [http://thingy.io](http://thingy.io) and detailed documentation under [http://docs.thingy.io](http://docs.thingy.io).

## Credits & licenses

Copyright (c) 2017 Thingy.IO
