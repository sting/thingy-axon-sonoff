/*
  Thingy Axon - Sonoff Basic

  This axon runs on Sonoff Basic (http://sonoff.itead.cc/en/products/sonoff/sonoff-basic) and connects to Thingy system

  Copyright (c) 2017 Thingy.IO
*/

#define   PIN_BUTTON             0
#define   PIN_INPUT              14
#define   PIN_LED                13
#define   PIN_RELAY              12

#include <Thingy.h>
#include <Ticker.h>
#include <Button.h>

typedef enum { NONE, ON, OFF, TOGGLE } Action;

Thingy::Axon sonoff;
Ticker ticker;
Button button(PIN_BUTTON);
Action action = NONE;

void tick()
{
  int state = digitalRead(PIN_LED);
  digitalWrite(PIN_LED, !state);
}

void on()
{
  Serial << F("ON") << endl;
  digitalWrite(PIN_RELAY, HIGH);
  digitalWrite(PIN_LED, LOW);
  SPIFFS.rename("/state.off", "/state.on");
  sonoff["State"].put("ON", 1, true);
}

void off()
{
  Serial << F("OFF") << endl;
  digitalWrite(PIN_RELAY, LOW);
  digitalWrite(PIN_LED, HIGH);
  SPIFFS.rename("/state.on", "/state.off");
  sonoff["State"].put("OFF", 1, true);
}

void toggle()
{
  Serial << F("TOGGLE") << endl;
  digitalRead(PIN_RELAY) == HIGH ? off() : on();
}

void handle_command(const Thingy::Stream& stream, const String& value)
{
  if (value == "ON")
    action = ON;
  if (value == "OFF")
    action = OFF;
  if (value == "TOGGLE")
    action = TOGGLE;
}

void setup()
{
	// Setup console
	Serial.begin(115200);
	delay(10);

	Serial << endl << endl  << F("Sonoff.Basic") << " - " << __DATE__ << " " << __TIME__ << endl << "#" << endl << endl;

  // LED
  pinMode(PIN_LED, OUTPUT);
  ticker.attach(0.6, tick);

  // Button
  button.begin();

  // Relay
  pinMode(PIN_RELAY, OUTPUT);

  // Thingy
  sonoff.setup();

  // Persisted state
  bool onoff = false;

  if (SPIFFS.exists("/state.on"))
  {
    Serial << F("Sonoff.Basic - ON") << endl;
    onoff = true;
    digitalWrite(PIN_RELAY, HIGH);
  }

  if (SPIFFS.exists("/state.off"))
  {
    Serial << F("Sonoff.Basic - OFF") << endl;
    onoff = false;
    digitalWrite(PIN_RELAY, LOW);
  }

  if (!sonoff.begin())
  {
    Serial << F("Sonoff.Basic - Fatal error. Please restart.") << endl << endl << endl;
    sonoff.restart();
  }

  ticker.detach();

	// setting handles
	sonoff["Command"].setHandle(handle_command);

  action = onoff ? ON : OFF;
}

void loop()
{
  sonoff.loop();

  if (button.released())
    action = TOGGLE;

  if (action == ON)
    on();
  if (action == OFF)
    off();
  if (action == TOGGLE)
    toggle();

  if (action != NONE)
    action = NONE;
}
